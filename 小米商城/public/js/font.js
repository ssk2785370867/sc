(function (doc, win) {
     var docEl = doc.documentElement,
          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
          recalc    = function () {
                 var clientWidth = docEl.clientWidth;
                 if (clientWidth>=640) {
                    clientWidth = 640;
                 };
                 if (!clientWidth) return;
               docEl.style.fontSize = 100 * (clientWidth / 640) + 'px'; //注意此处以1rem=100px为基准
          };
          if (!doc.addEventListener) return;
          win.addEventListener(resizeEvt, recalc, false);
          doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);