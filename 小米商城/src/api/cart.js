import http from "./request.js"


// 获取数据
const list = async (msg) => {
    let data = await http({
        url: 'user/shoppingCart/getShoppingCart',
        method: 'post',
        data: msg,
    })
    return data
}


const plusNum = async (msg) => {
    let data = await http({
        url: `user/shoppingCart/updateShoppingCart`,
        method: 'post',
        data: msg
    })
    return data
}



const delList = async(msg)=>{
    let data = await http({
        url:'user/shoppingCart/deleteShoppingCart',
        method:'post',
        data:msg
    })
    return data
}

export { list, plusNum ,delList}

