import http from "./request.js"


const getlist = async (msg) => {
    let data = await http({
        url: 'product/getDetails',
        method: 'post',
        data: msg,
    })
    return data
}

const getSwiper = async (msg) => {
    let data = await http({
        url: 'product/getDetailsPicture',
        method: 'post',
        data: msg,
    })
    return data
}

const addList = async (msg) => {
    let data = await http({
        url: 'user/shoppingCart/addShoppingCart',
        method: 'post',
        data: msg
    })
    return data
}

const addLike = async (msg) => {
    let data = await http({
        url: 'user/collect/addCollect',
        method: 'post',
        data: msg
    })
    return data
}


export { getlist, getSwiper, addList, addLike }