import http from "./request.js"

function getSwiperdata() {// 轮播图数据
    return http({
        url: 'resources/carousel',
        method: 'post'
    })
}

// ==============================这是一条帅气的分界线（手机数据）==============================


function getMobiledata() {
    return http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: '手机' }
    })
}

// ==============================这是一条帅气的分界线（家电数据）==============================


function getHouseholddata() {
    return http({
        url: 'product/getHotProduct',
        method: 'post',
        data: { categoryName: ["电视机", "空调", "洗衣机"] }
    })
}
// 家电里面的电影影视数据
function getMovies() {
    return http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: "电视机" }
    })
}

// ==============================这是一条帅气的分界线（配件数据）==============================


function getPartsdata() {// 配件热门数据
    return http({
        url: 'product/getHotProduct',
        method: 'post',
        data: { categoryName: ["保护套", "保护膜", "充电器", "充电宝"] }
    })
}

function getChargerdata() {// 配件充电器数据
    return http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: "充电器" }
    })
}
// 
function getprotectdata() {// 配件保护套数据
    return http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: "保护套" }
    })
}




// ==============================这是一条帅气的分界线（全部商品数据）==============================
// 全部商品头部数据

function gettopListdata() {// 配件保护套数据
    return http({
        url: 'product/getCategory',
        method: 'post',
    })
}



function getlistdata() {// 配件保护套数据
    return http({
        url: 'product/getAllProduct',
        method: 'post',
    })
}



function searchList(msg) {
    return http({
        url: 'product/getProductBySearch',
        method: 'post',
        data: msg
    })
}


// ==============================这是一条帅气的分界线（关于我们数据）==============================
// 
function getMydata() {// 配件保护套数据
    return http({
        url: 'docs/README.md',
        method: 'get',
    })
}


// 注册
function zc(msg) {
    return http({
        url: 'users/register',
        method: 'post',
        data: msg,
    })
}

// 登录
function dl(msg) {
    return http({
        url: 'users/login',
        method: 'post',
        data: msg
    })
}



function filterList(msg) {
    return http({
        url: 'product/getProductByCategory',
        method: 'post',
        data: msg
    })
}



// ==============================这是一条帅气的分界线==============================



// ==============================这是一条帅气的分界线==============================


// ==============================这是一条帅气的分界线==============================


export {
    getSwiperdata,
    getMobiledata,
    getHouseholddata,
    getPartsdata,
    getMovies,
    getChargerdata,
    getprotectdata,
    gettopListdata,
    getlistdata,
    getMydata,
    zc,
    dl,
    searchList,
    filterList
}