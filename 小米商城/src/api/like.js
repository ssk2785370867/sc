import http from "./request.js"

const getlist = async (msg) => {
    let data = await http({
        url: 'user/collect/getCollect',
        method: 'post',
        data: msg
    })
    return data
}

const delList = async (msg)=>{
    let data = await http({
        url:'user/collect/deleteCollect',
        method:'post',
        data:msg
    })
    return data
}

export { getlist,delList }