import http from "./request.js"



const getlist = async (msg)=>{
    let data = await http({
        url:'user/order/getOrder',
        method:'post',
        data:msg
    })
    return data
}

export {getlist}