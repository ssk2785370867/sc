import http from "./request.js"



const addOrder = async (msg)=>{
    let data = await http({
        url:'user/order/addOrder',
        method:'post',
        data:msg
    })
    return data
}
export {addOrder}