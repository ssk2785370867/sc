import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import mavonEditor from 'mavon-editor'


Vue.use(ElementUI);
Vue.use(mavonEditor)






Vue.config.productionTip = false


router.beforeEach((to, from, next) => {
  // 购物车拦截
  if (to.path === '/cart') {
    const id = localStorage.getItem('user_id')
    if (!id) {
      return next('/App')
    }
  }
  // 收藏页面拦截
  if (to.path === '/like') {
    
    const id = localStorage.getItem('user_id')
    if (!id) {
      return next('/App')
    }
  }
  next()
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
