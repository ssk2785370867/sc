import Vue from "vue";
import Router from "vue-router";
import goods from "@/views/goods/goods.vue";


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/goods",
      name: "goods",
      component: goods
    },
    {
      path: "/seller",
      name: "seller",
       component: () =>
       import("@/views/seller/seller.vue")
    },
    {
      path: "/ratings",
      name: "ratings",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "ratings" */"@/views/ratings/ratings.vue")
    },
    {
      path: "/details",
      name: "details",
      component: () =>
        import("@/views/details/details.vue")
    },
    {
      path: "/cart",
      name: "cart",
      component: () =>
        import("@/views/cart/cart.vue")
    },
    {
      path: "/like",
      name: "like",
      component: () =>
        import("@/views/like/like.vue")
    },
    {
      path: "/order",
      name: "order",
      component: () =>
        import("@/views/order/order.vue")
    },
    {
      path: "/myorder",
      name: "myorder",
      component: () =>
        import("@/views/myorder/myorder.vue")
    },
    {
      path: "/",
      redirect: '/goods'
    },
    {
      path: "*",
      redirect: '/goods'
    },
  ]
});
